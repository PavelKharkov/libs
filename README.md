# USEFUL LIBS, TOOLS & FRAMEWORKS #
  
## UI ##
  * [Bootstrap](https://github.com/twbs/bootstrap) - css, html & js framewrok. Requires **jQuery**
  * [Foundation](https://github.com/zurb/foundation-sites) - advasnced responsive front-end framework
  * [UI Kit](https://github.com/uikit/uikit) - a lightweight and modular front-end framework
  * [Semantic UI](https://github.com/semantic-org/semantic-ui/) - component framework
  * [jQuery UI](https://github.com/jquery/jquery-ui) - official jQuery ui library. Requires **jQuery**
  
## Javascript UI ##
  * [Dojo Toolkit](https://github.com/dojo) - javascript UI toolkit
  
## Accessibility ##
  * [HTML5 Accessibility](http://www.html5accessibility.com/) - current accessibility support status of HTML5 features across major browsers.
  * [accessible datepciker example](http://www.oaa-accessibility.org/examplep/datepicker1/) ([implementation with Bootstrap](http://eureka2.github.io/ab-datepicker/))
  
---
## Utilities ##
  * [Keycode.info](http://keycode.info/) - press key to get JavaScript keycode value
  * [EventBus](https://github.com/krasimir/EventBus) - simple custom events manager
  * [GSgen.ru](http://gsgen.ru/tools/names-nicks-list/) - name lists generator

## Platform & browser detection ##
  * [Platform.js](https://github.com/bestiejs/platform.js/) - platform detection library
  * [Mobile-detect](https://github.com/hgoebl/mobile-detect.js) - mobile device type detection
  * [Modernizr](https://github.com/Modernizr/Modernizr) - detect HTML5 and CSS3 features in the user’s browser
  * [Detectizr](https://github.com/barisaydinoglu/Detectizr) - a Modernizr extension to detect device, device model, screen size, operating system, and browser details. Requires: **Modernizr**
  
---
## Animation ##
  * [Easing Functions Cheat Sheet](https://easings.net/en)
  * [GSAP](https://github.com/greensock/GreenSock-JS/) - large animation library
  * [Velocity.js](http://github.com/julianshapiro/velocity) - accelerated JavaScript animation
  * [Anime.js](https://github.com/juliangarnier/anime) - javaScript animation engine
  * [Animate.css](https://github.com/daneden/animate.css) - css animations library
  * [Motio](https://github.com/darsain/motio) - library for sprite based animations and panning
  * [Vivus.js](https://github.com/maxwellito/vivus#vivusjs) - library to make drawing animation on SVG

## Cookies ##
  * [js-cookie](https://github.com/js-cookie/js-cookie) - simple, lightweight js cookie management
  
## Colors ##
  * [Reshader](https://github.com/chiefGui/reshader) - library to get shades of colors
  
## Date ##
  * [Moment.js](https://github.com/moment/moment/) - parse, validate, manipulate, and display dates in javascript
  * [Pikaday](https://github.com/dbushell/Pikaday) - Lightweight datepciker, modular CSS. Has jQuery version.
  * [Rome](https://github.com/bevacqua/rome) - Customizable date (and time) picker.
  * [jQuery UI Datepicker](https://jqueryui.com/datepicker/) - jQuery UI datepicker. Requires: **jQuery**
  * [Datepicker](https://github.com/fengyuanchen/datepicker) - simple datepicker plugin. Requires: **jQuery**
  
## Drag, drop, sort ##
  * [Muuri](https://github.com/haltu/muuri) - responsive, sortable & draggable layouts
  * [Sortable](https://github.com/RubaXa/Sortable) - drag-and-drop sortable elements
  * [Hammer.js](http://hammerjs.github.io/) - touch gestures support
  
## Favicons ##
  * [https://github.com/audreyr/favicon-cheat-sheet] - obsessive cheat sheet to favicon sizes/types.
  * [Favico.js](http://lab.ejci.net/favico.js/) - dynamic favicons
  
## File upload ##
  * [MultiFile](https://github.com/fyneworks/multifile) - jQuery Multiple File Selection Plugin. Requires: **jQuery**
  
## Input masks ##
  * [Jquery mask plugin](https://github.com/igorescobar/jQuery-Mask-Plugin) - Requires: **jQuery**
  * [Inputmask](https://github.com/RobinHerbots/Inputmask) - Vanilla js mask plugin. Needs testing
  * [Cleave.js](http://nosir.github.io/cleave.js/) - Vanilla input mask. Needs testing

## Popups ##
  * [Magnific Popup](https://github.com/dimsemenov/Magnific-Popup) - responsive inline, gallery or iframe popups. Requires: **jQuery**
  * [SweetAlert2](https://limonte.github.io/sweetalert2/) - alert popups
  
## Range slider ##
  * [noUISlider](https://github.com/leongersen/noUiSlider/) - lightweight range slider library
  * [ion.rangeSlider](https://github.com/IonDen/ion.rangeSlider) - range slider. Requires: **jQuery**
  * [jQuery UI slider](https://jqueryui.com/slider/) - jQuery UI range slider. Requires: **jQuery**
  
## Selectboxes ##
  * [Choices](https://github.com/jshjohnson/Choices) - vanilla customisable select box/text input
  * [Bootstrap select](https://github.com/silviomoreto/bootstrap-select) - customizable selectbox which uses Bootstrap's dropdown element. Requires: **jQuery**, **Bootstrap**
  * [Select2](https://github.com/select2) - customizable selectbox. Requires: **jQuery**
  * [Chosen](https://github.com/harvesthq/chosen) - custimizable selectbox. Requires: **jQuery**
  * [Selectize.js](https://github.com/selectize/selectize.js) - hybrid between selectbox & textbox. Requires: **jQuery**
  
## Sliders ##
  * [OwlCarousel2](https://github.com/OwlCarousel2/OwlCarousel2) - jQuery Responsive Carousel. Requires: **jQuery**
  * [Sly](https://github.com/darsain/sly) - javaScript library for one-directional scrolling with item based navigation support. **Outdated**
  
## Social ##
  * [AddToAny](https://www.addtoany.com/buttons/for/website) - share buttons for different services
  
## SVG ##
  * [Snap.svg](https://github.com/adobe-webplatform/Snap.svg) - the JavaScript library for modern SVG graphics
  * [ICONSVG](https://iconsvg.xyz/) - quick customizable SVG icons
  * [Ikonate](https://www.ikonate.com/) - fully customisable & accessible vector icons
  * [Tabler Icons](https://tablericons.com/) - 1298 fully customizable SVG icons

## Tooltips ##
  * [Popper.js](https://popper.js.org/) - tooltip library. Used in Bootstrap's tooltips, popovers & dropdowns
  * [Tether](https://github.com/HubSpot/tether) - tooltip library
  
## Form validation ##
  * [Validate.js](https://github.com/ansman/validate.js) - vanilla js validation
  * [jQuery validation](https://jqueryvalidation.org/) - simple but customizable form validation. Requires: **jQuery**
  
## Page transition ##
  * [Barba.js](https://github.com/luruke/barba.js) - smooth transitions between pages
  * [Page.js](https://visionmedia.github.io/page.js/) - micro client-side router inspired by the Express router
  * [fullPage.js](https://github.com/alvarotrigo/fullPage.js) - create full screen pages fast and simple
  * [Animsition](https://github.com/blivesta/animsition) - a simple and easy jQuery plugin for CSS animated page transitions
  
## Scroll ##
  * [Midnight.js](https://github.com/Aerolab/midnight.js) - switch fixed element styles on the fly
  * [AOS](https://github.com/michalsnik/aos) - animate on scroll library
  * [ScrollMagic](https://github.com/janpaepke/ScrollMagic) - library for scroll interactions
  * [ScrollReveal](https://github.com/jlmakes/scrollreveal) - animations on scroll
  * [Parallax.js](https://github.com/wagerfield/parallax) - parallax Engine that reacts to the orientation of a smart device
  * [Scrolline.js](https://github.com/anthonyly/Scrolline.js) - create a scroll line bar indicator on the page. Requires: **jQuery**
  * [Basic forkable fiddle to showcase ScrollMagic issues.](http://jsfiddle.net/xPusHx/svbg6xyh/)
  
## Other ##
  * [Easyzoom](https://github.com/i-like-robots/EasyZoom) - image zoom plugin. Requires: **jQuery**
  * [TwentyTwenty](https://zurb.com/playground/twentytwenty) - image visual diff tool. Requires: **jQuery**
  * [Highlight.js](https://highlightjs.org/) - syntax highlighting for the Web
  * [LuminJS](https://lumin.rocks/) - library to progressively highlight any text on a page
  
## Polyfills ##
  * [svg4everybody](https://github.com/jonathantneal/svg4everybody) - external svg poylfill for IE
  
---
## React ##
  * [ReactSymbols](http://docs.reactsymbols.com/#/?id=documentation) - simple React components
  
---
## Vanilla JS ##
  * [Vanilla JavaScript slideUp and slideDown functions](https://gist.github.com/tannerhodges/fcb758a0997f997a2adb3d1750023523)
  * [JavaScript slidedown without jQuery](https://stackoverflow.com/questions/3795481/javascript-slidedown-without-jquery?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa)
  * [Simple vanilla js calendar](https://codepen.io/snypelife/pen/LtBam)
  
---
## Vue ##
  * [Authentication Best Practices for Vue](https://blog.sqreen.io/authentication-best-practices-vue/)
  
---
## Animation ##
  * [How to create the snake highlight animation with anime-js](https://blog.prototypr.io/how-to-create-the-snake-highlight-animation-with-anime-js-bf9c6cb66434)
  * [Particle Effects for Buttons](https://tympanus.net/Development/ParticleEffectsButtons/)
  * [Stunning hover effects with CSS variables](https://blog.prototypr.io/stunning-hover-effects-with-css-variables-f855e7b95330)
  * [Subtle Click Feedback Effects](https://tympanus.net/Development/ClickEffects/)
  * [CSS Menu Icon Animation: Know Your Menu](https://codepen.io/oliviale/pen/gKParr)
  * [Dot Menu Animations](https://codepen.io/Zaku/details/YjRqzB/)
  * [Svg animate along path with rotation](https://codepen.io/graphilla/pen/VmbGgm)
  
---
## SVG ##
  * [SVG маски и вау-эффекты: о магии простыми словами](https://habrahabr.ru/post/349362/)
  
---
## Tools ##
  * [Генератор ников, имён, отчеств и фамилий](http://gsgen.ru/tools/names-nicks-list/)
  * [Генератор случайных номеров телефона](https://www.calc.ru/generator-sluchaynykh-nomerov-telefona.html)
---
## Tests ##
  * [Тестирование HTML-верстки](https://www.100up.ru/testirovanie-html-verstki/)
  * [Тестирование скорости сайта](https://www.thinkwithgoogle.com/feature/testmysite)